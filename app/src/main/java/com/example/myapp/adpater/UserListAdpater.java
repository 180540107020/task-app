package com.example.myapp.adpater;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.example.myapp.R;
import com.example.myapp.util.Const;

import java.util.ArrayList;
import java.util.HashMap;

public class UserListAdpater extends BaseAdapter {

    Context context;
    ArrayList<HashMap<String, Object>> userList;


    @RequiresApi(api = Build.VERSION_CODES.M)
    public UserListAdpater(Context context, ArrayList<HashMap<String, Object>> userList) {
        this.context = context;
        this.userList = userList;
    }

    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Object getItem(int i) {
        return userList.get(i);

    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder ViewHolder;
        View v = view;
        if (v == null) {
            v = LayoutInflater.from(context).inflate(R.layout.view_row_user_list, null);
            ViewHolder = new ViewHolder();
            ViewHolder.tvName = ((TextView) v.findViewById(R.id.tvLstName));
            ViewHolder.tvEmail = ((TextView) v.findViewById(R.id.tvLstEmail));
            ViewHolder.tvGender = ((TextView) v.findViewById(R.id.tvLstGender));
            v.setTag(ViewHolder);
        } else {
            ViewHolder = (ViewHolder) view.getTag();
        }
        ViewHolder.tvName.setText(userList.get(position).get(Const.FIRST_NAME) + "" + userList.get(position).get(Const.LAST_NAME));
        ViewHolder.tvEmail.setText(String.valueOf(userList.get(position).get(Const.EMAIL_ADDRESS)));
        ViewHolder.tvGender.setText(String.valueOf(userList.get(position).get(Const.GENDER)));
        return v;
    }
    class ViewHolder {
        TextView tvName;
        TextView tvEmail;
        TextView tvGender;
    }
}
