package com.example.myapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapp.R;
import com.example.myapp.database.Mydatabase;
import com.example.myapp.util.Const;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    EditText etFirstName, etLastName, etEmail;
    final ArrayList<HashMap<String, Object>> userList = new ArrayList<>();
    TextView tvDisplay;
    boolean doubleBackToExit = false;
    RadioGroup rgGender;
    RadioButton rbMale;
    RadioButton rbFeMale;

    CheckBox chbActCricket;
    CheckBox chbActFootball;
    CheckBox chbActHockey;
    Button btnSubmit;
    Button btnName;
    Button btnLastname;
    Button btnEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViewReference();
        initViewEvent();
    }

    void initViewEvent() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {
                    HashMap<String, Object> map = new HashMap<>();
                    map.put(Const.FIRST_NAME, etFirstName.getText().toString());
                    map.put(Const.LAST_NAME, etLastName.getText().toString());
                    map.put(Const.EMAIL_ADDRESS, etEmail.getText().toString());
                    map.put(Const.GENDER, rbMale.isChecked() ? rbMale.getText().toString() : rbFeMale.getText().toString());
                    String hobbies = "";
                    if (chbActCricket.isChecked()) {
                        hobbies += "," + chbActCricket.getText().toString();
                    }
                    if (chbActFootball.isChecked()) {
                        hobbies += "," + chbActFootball.getText().toString();
                    }
                    if (chbActHockey.isChecked()) {
                        hobbies += "," + chbActHockey.getText().toString();
                    }
                    map.put(Const.HOBBY, hobbies);
                    userList.add(map);

                    Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                    intent.putExtra("UserList", userList);
                    startActivity(intent);

                    Toast.makeText(getApplicationContext(), rbMale.isChecked() ? "Male" : "Female", Toast.LENGTH_SHORT).show();
                    etFirstName.setText("");
                    etLastName.setText("");
                    etEmail.setText("");
                }
            }
        });
    }

    /* Typeface typeface = Typeface.createFromAsset(getAssets(), "Grandstander-Italic-VariableFont_wght.ttf");

        btnSubmit.setTypeface(typeface);
        tvDisplay.setTypeface(typeface);
        etFirstName.setTypeface(typeface);
        etLastName.setTypeface(typeface);
    }*/
    void initViewReference() {

        new Mydatabase(MainActivity.this).getReadableDatabase();
        etFirstName = findViewById(R.id.etActFirstName);
        etLastName = findViewById(R.id.etActLastName);
        etEmail = findViewById(R.id.etActEmail);
        btnSubmit = findViewById(R.id.btnActSubmit);
        btnName = findViewById(R.id.btnActName);
        btnLastname = findViewById(R.id.btnActLastName);
        btnEmail = findViewById(R.id.btnActEmail);
        tvDisplay = findViewById(R.id.tvActDisplay);


        rgGender = findViewById(R.id.rgActGender);
        rbMale = findViewById(R.id.rbActMale);
        rbFeMale = findViewById(R.id.rbActFemale);

        chbActCricket = findViewById(R.id.chbActCricket);
        chbActFootball = findViewById(R.id.chbActFootball);
        chbActHockey = findViewById(R.id.chbActHockey);

        getSupportActionBar().setTitle(R.string.lbl_mainActivity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                if (i == R.id.rbActMale) {
                    chbActCricket.setVisibility(View.VISIBLE);
                    chbActFootball.setVisibility(View.VISIBLE);
                    chbActHockey.setVisibility(View.VISIBLE);
                } else if (i == R.id.rbActFemale) {
                    chbActCricket.setVisibility(View.VISIBLE);
                    chbActFootball.setVisibility(View.VISIBLE);
                    chbActHockey.setVisibility(View.GONE);
                }
            }
        });

        btnName.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                etFirstName.getText().clear();
            }
        });
        btnLastname.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                etLastName.getText().clear();
            }
        });
        btnEmail.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                etEmail.getText().clear();
            }
        });
    }

    //Option menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dashboard_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_1) {
            //Toast.makeText(MainActivity.this,"menu clicked",Toast.LENGTH_SHORT).show();
        } else if (item.getItemId() == R.id.menu_2) {
        } else if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    //double click and app closed
    public void onBackPressed() {
        if (doubleBackToExit) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExit = true;
        Toast.makeText(this, "please click back again and then exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExit = false;
            }
        }, 2000);
    }

    //Validation
    boolean isValid() {
        boolean flag = true;
        //first name validation
        if (TextUtils.isEmpty(etFirstName.getText().toString().trim())) {
            etFirstName.setError(getString(R.string.error_enter_value));
            flag = false;
        } else {
            String namePattern = "[a-zA-Z.\\s]+";
            String name = etFirstName.getText().toString().trim();
            if (!name.matches(namePattern)) {
                etFirstName.setError("enter valid name");
                flag = false;
            }
        }
        //last name validation
        if (TextUtils.isEmpty(etLastName.getText())) {
            etLastName.setError(getString(R.string.error_enter_value));
            flag = false;
        } else {
            String MobilePattern = "[0-9]{10}";
            String phoneNumber = etLastName.getText().toString();
            if (phoneNumber.length() < 10 && !phoneNumber.matches(MobilePattern)) {
                etLastName.setError("enter valid number");
                flag = false;
            }
        }
        //email validation
        if (TextUtils.isEmpty(etEmail.getText())) {
            etEmail.setError(getString(R.string.error_enter_value));
            flag = false;
        } else {
            String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
            String email = etEmail.getText().toString().trim();
            if (!email.matches(emailPattern)) {
                etEmail.setError("enter valid email");
                flag = false;
            }
        }
        //checkbox button
        if (!(chbActCricket.isChecked() || chbActFootball.isChecked() || chbActHockey.isChecked())) {
            Toast.makeText(this, "plz select any choice", Toast.LENGTH_SHORT).show();
            flag = false;
        }
        return flag;
    }
}